package com.plutonii.masters.degre.l1;

import java.io.*;
import java.util.*;

public class Graph {

    static boolean orientedGraph = false;
    static boolean weightedGraph = false;
    static boolean loopsInGraph = false;
    static int vertexCount = 0;
    static int vertexMin = 0;
    static int vertexMax = 0;
    static int edgesCount = 0;
    static boolean negativeWeight = false;
    static List<Integer> weightsEdges = new ArrayList<>();

    static Map<Integer, Map<Integer, Integer>> dictGraph = new HashMap<>();
    static List<int[]> adjacentPairs = new ArrayList<>();
    static int[][] adjacentMatrix;
    static int[][] incidenceMatrix;

    public static void main(String[] args) throws IOException {
        readFile();
        showDataGraph();
        graphDictVertex();
        createGraph();
        createAdjMatrix();
        addEdgeInMatrix();
        printMatrix();
        saveAdjMatrixToFile("adj_matrix.txt");
        printAdjPairs();
        saveAdjListToFile("adj_list.txt");
        printIncidenceMatrix();
        printDegreeVertex();
        findLoops();
        printLoops();
        printParallelEdges();
    }

    public static void readFile() throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(RandomGraphGeneratorOld.class.getResourceAsStream("/params.txt"))));
        String[] firstLine = reader.readLine().split(" ");
        orientedGraph = Integer.parseInt(firstLine[0]) == 1;
        weightedGraph = Integer.parseInt(firstLine[1]) == 1;
        loopsInGraph = Integer.parseInt(firstLine[2]) == 1;

        String[] secondLine = reader.readLine().split(" ");
        vertexMin = Integer.parseInt(secondLine[0]);
        vertexMax = Integer.parseInt(secondLine[1]);
        vertexCount = new Random().nextInt(vertexMax - vertexMin + 1) + vertexMin;

        String[] thirdLine = reader.readLine().split(" ");
        int edgesMin = Integer.parseInt(thirdLine[0]);
        int edgesMax = Integer.parseInt(thirdLine[1]);
        edgesCount = new Random().nextInt(edgesMax - edgesMin + 1) + edgesMin;

        if (weightedGraph) {
            String[] fourthLine = reader.readLine().split(" ");
            int weightsMin = Integer.parseInt(fourthLine[0]);
            int weightsMax = Integer.parseInt(fourthLine[1]);

            for (int i = 0; i < edgesCount; i++) {
                int randomWeight;
                do {
                    randomWeight = new Random().nextInt(weightsMax - weightsMin + 1) + weightsMin;
                    if (randomWeight < 1) negativeWeight = true;
                } while (randomWeight == 0);
                weightsEdges.add(randomWeight);
            }
        }

        reader.close();
    }

    public static void showDataGraph() {
        System.out.println("\nГраф является:");
        System.out.println(orientedGraph ? "- ориентированным" : "- неориентированным");
        System.out.println(weightedGraph ? "- взвешенным" : "- невзвешенным");
        System.out.println(loopsInGraph ? "- с петлями" : "- без петель");
        System.out.println("\nВ графе: вершин - " + vertexCount + "; ребер - " + edgesCount);
    }

    public static void graphDictVertex() {
        for (int i = 0; i < vertexCount; i++) {
            dictGraph.put(i + 1, new HashMap<>());
        }
    }

    public static void createGraph() {
        Random random = new Random();
        for (int i = 0; i < edgesCount; i++) {
            int currentVertex, adjacentVertex;
            do {
                currentVertex = random.nextInt(vertexCount) + 1;
                adjacentVertex = random.nextInt(vertexCount) + 1;

                if (!loopsInGraph && currentVertex == adjacentVertex) continue;
                if (dictGraph.get(currentVertex).containsKey(adjacentVertex)) continue;

                break;
            } while (true);

            if (weightedGraph) {
                dictGraph.get(currentVertex).put(adjacentVertex, weightsEdges.get(i));
            } else {
                dictGraph.get(currentVertex).put(adjacentVertex, 1);
            }
        }
    }

    public static void createAdjMatrix() {
        adjacentMatrix = new int[vertexCount][vertexCount];
    }

    public static void printMatrix() {
        System.out.println("\nМатрица смежности:");
        for (int[] row : adjacentMatrix) {
            for (int value : row) {
                System.out.print(String.format("%-3d", value));
            }
            System.out.println();
        }
    }

    public static void addEdgeInMatrix() {
        for (Map.Entry<Integer, Map<Integer, Integer>> entry : dictGraph.entrySet()) {
            int i = entry.getKey();
            for (Map.Entry<Integer, Integer> adjacent : entry.getValue().entrySet()) {
                int j = adjacent.getKey();
                int weight = adjacent.getValue();
                adjacentMatrix[i - 1][j - 1] = weight;
                if (!orientedGraph) {
                    adjacentMatrix[j - 1][i - 1] = weight;
                }
            }
        }
    }

    public static List<int[]> findAdjacentPairs(Map<Integer, Map<Integer, Integer>> graph) {
        List<int[]> pairs = new ArrayList<>();
        for (Map.Entry<Integer, Map<Integer, Integer>> entry : graph.entrySet()) {
            int vertex = entry.getKey();
            for (int adjacentVertex : entry.getValue().keySet()) {
                pairs.add(new int[]{vertex, adjacentVertex});
            }
        }
        return pairs;
    }

    public static void printAdjPairs() {
        System.out.println("\nСписок упорядоченных пар смежных вершин:");
        adjacentPairs = findAdjacentPairs(dictGraph);
        for (int[] pair : adjacentPairs) {
            System.out.println(pair[0] + " - " + pair[1]);
        }
    }

    public static int[][] buildIncidenceMatrix(List<int[]> graph) {
        int[][] incidenceMatrix = new int[vertexCount][edgesCount];
        int count = 0;
        for (int[] edge : graph) {
            incidenceMatrix[edge[0] - 1][count] = 1;
            incidenceMatrix[edge[1] - 1][count] = 1;
            count++;
        }
        return incidenceMatrix;
    }

    public static void printIncidenceMatrix() {
        System.out.println("\nМатрица инцидентности ребер и вершин:");
        incidenceMatrix = buildIncidenceMatrix(adjacentPairs);
        for (int[] row : incidenceMatrix) {
            for (int value : row) {
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }

    public static Map<Integer, Integer> degreeUndirected(Map<Integer, Map<Integer, Integer>> graph) {
        Map<Integer, Integer> degrees = new HashMap<>();
        for (Integer vertex : graph.keySet()) {
            degrees.put(vertex, graph.get(vertex).size());
        }
        return degrees;
    }

    public static Map<String, Map<Integer, Integer>> degreeDirected(Map<Integer, Map<Integer, Integer>> graph) {
        Map<Integer, Integer> inDegrees = new HashMap<>();
        Map<Integer, Integer> outDegrees = new HashMap<>();
        for (Integer vertex : graph.keySet()) {
            outDegrees.put(vertex, graph.get(vertex).size());
            inDegrees.put(vertex, 0);
        }
        for (Integer vertex : graph.keySet()) {
            for (Integer neighbor : graph.get(vertex).keySet()) {
                inDegrees.put(neighbor, inDegrees.get(neighbor) + 1);
            }
        }
        Map<String, Map<Integer, Integer>> degrees = new HashMap<>();
        degrees.put("inDegrees", inDegrees);
        degrees.put("outDegrees", outDegrees);
        return degrees;
    }

    public static void printDegreeVertex() {
        if (!orientedGraph) {
            Map<Integer, Integer> undirectedDegrees = degreeUndirected(dictGraph);
            System.out.println("\nСтепени вершин в неориентированном графе:");
            for (Map.Entry<Integer, Integer> entry : undirectedDegrees.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        } else {
            Map<String, Map<Integer, Integer>> degrees = degreeDirected(dictGraph);
            Map<Integer, Integer> inDegrees = degrees.get("inDegrees");
            Map<Integer, Integer> outDegrees = degrees.get("outDegrees");
            System.out.println("\nСтепени вершин в ориентированном графе:");
            for (Integer vertex : dictGraph.keySet()) {
                System.out.println(vertex + " Входящая степень - " + inDegrees.get(vertex) + ", Исходящая степень - " + outDegrees.get(vertex));
            }
        }
    }

    static List<Integer> loops = new ArrayList<>();

    public static void findLoops() {
        for (int i = 0; i < vertexCount; i++) {
            if (adjacentMatrix[i][i] != 0) {
                loops.add(i + 1);
            }
        }
    }

    public static void printLoops() {
        System.out.println("\nПетли в графе на вершинах:");
        if (!loops.isEmpty()) {
            for (int loop : loops) {
                System.out.println(loop);
            }
        } else {
            System.out.println("В графе нет петель");
        }
    }

    public static void notOrientedParallelEdges() {
        List<List<Integer>> duplicate = new ArrayList<>();
        Set<Set<Integer>> seenPairs = new HashSet<>();
        for (int[] pair : adjacentPairs) {
            Set<Integer> sortedPair = new HashSet<>(Arrays.asList(pair[0], pair[1]));
            if (seenPairs.contains(sortedPair)) {
                duplicate.add(new ArrayList<>(sortedPair));
            }
            seenPairs.add(sortedPair);
        }

        if (!duplicate.isEmpty()) {
            System.out.println("Параллельные ребра есть между вершинами:");
            for (List<Integer> edge : duplicate) {
                System.out.println(edge.get(0) + " и " + edge.get(1));
            }
        } else {
            System.out.println("В графе нет параллельных ребер");
        }
    }

    public static void orientedParallelEdges() {
        List<int[]> parallelEdges = new ArrayList<>();
        Set<String> visitedEdges = new HashSet<>();

        for (Map.Entry<Integer, Map<Integer, Integer>> entry : dictGraph.entrySet()) {
            int node = entry.getKey();
            for (Map.Entry<Integer, Integer> neighborEntry : entry.getValue().entrySet()) {
                int neighbor = neighborEntry.getKey();
                String edge = node + "-" + neighbor;

                if (visitedEdges.contains(edge) || visitedEdges.contains(neighbor + "-" + node)) {
                    parallelEdges.add(new int[]{node, neighbor});
                }

                visitedEdges.add(edge);
            }
        }

        if (!parallelEdges.isEmpty()) {
            System.out.println("\nПараллельные ребра:");
            for (int[] edge : parallelEdges) {
                System.out.println(edge[0] + " и " + edge[1]);
            }
        } else {
            System.out.println("В графе нет параллельных ребер");
        }
    }

    public static void printParallelEdges() {
        if (orientedGraph) {
            orientedParallelEdges();
        } else {
            notOrientedParallelEdges();
        }
    }

    public static void saveAdjMatrixToFile(String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (int[] row : adjacentMatrix) {
                for (int value : row) {
                    writer.write(value + " ");
                }
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Ошибка при записи матрицы смежности в файл: " + e.getMessage());
        }
    }

    public static void saveAdjListToFile(String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            for (Map.Entry<Integer, Map<Integer, Integer>> entry : dictGraph.entrySet()) {
                int vertex = entry.getKey();
                // writer.write();
                for (Map.Entry<Integer, Integer> adjacent : entry.getValue().entrySet()) {
                    writer.write(vertex + " - " + adjacent.getKey() + ", "/* + adjacent.getValue() + ") "*/);
                }
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Ошибка при записи списка смежности в файл: " + e.getMessage());
        }
    }
}
